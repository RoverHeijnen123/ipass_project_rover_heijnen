<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="ipass.domain.service.ServiceProvider"%>
<%@page import ="ipass.dao.domain.gebruikers" %>
<% gebruikers g = (gebruikers) request.getSession().getAttribute("loggedUser"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">
<script src="https://code.jquery.com/jquery-2.2.3.js"></script>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>
<section class="container">
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; margin-bottom: 0px;">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<a class="navbar-brand" href="#">Toernooi management Systeem</a>
    		</div>
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="index.jsp">Home</a></li>
			    <li><a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
			    <li><a href="toernooi_lijst.jsp">Toernooi lijst</a></li> 
    		</ul>
    		<ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-user"></span>User: <%=g.getGebruikersnaam() %> </a></li>
		      <li><span class="glyphicon glyphicon-log-out"></span><form action="LogoutServlet.do" method="get"><input class="glyphicon glyphicon-log-out" type="submit" name="logout" value="logout"></form></li>
    		</ul>
  		</div>
	</nav>
	
	<ul class="list-group">
	<% request.setAttribute("toernooiList", ServiceProvider.getToernooiService().getToernooiList()); %>
	  <c:forEach var="toernooi" items="${toernooiList}">
	  	<li class="list-group-item"><span class="badge">${toernooi.grote}</span><button type="button" class="btn btn-info" style="margin-left: 10px">${toernooi.naam}</button></li>
	  </c:forEach>
	</ul>
        
</section>

<script>
		$(document)
				.ready(
						function() {
							$('button').on('click', function() {
								// get the button that was clicked
								var buttonClicked = $(this).html();
								console.log(buttonClicked);
								sessionStorage.setItem("toernooi", buttonClicked);

								window.location.href = 'mijn_toernooi.jsp';

							});
						});
	</script>
</body>
</html>