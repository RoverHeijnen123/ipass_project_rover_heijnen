<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import ="ipass.dao.domain.gebruikers" %>
<% gebruikers g = (gebruikers) request.getSession().getAttribute("loggedUser"); %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>

<section class="container">
	<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<a class="navbar-brand" href="#">Toernooi management Systeem</a>
    		</div>
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="#">Home</a></li>
			    <li><a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
			    <li><a href="toernooi_lijst.jsp">Toernooi lijst</a></li> 
    		</ul>
    		<ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-user"></span>User: <%=g.getGebruikersnaam() %> </a></li>
		      <li><span class="glyphicon glyphicon-log-out"></span><form action="LogoutServlet.do" method="get"><input class="glyphicon glyphicon-log-out" type="submit" name="logout" value="logout"></form></li>
    		</ul>
  		</div>
	</nav>
	
	<div class="row">
          <section class='col-xs-12 col-sm-6 col-md-6'>
            <section>
              <h2>Wat is het Toernooi Management Systeem?</h2>
                <p>Het toernooi management systeem is een makkelijke tool waarmee je met je vrienden makkelijk toernoois kan orginiseren. Dit kan voor games zijn maar ook voetbal. Het heeft geen regels maar hou het wel netjes, het systeem is vooral bedoeld voor vrienden groepen.</p>
                  <ul>
                    <li>Wil je meedoen aan een toernooi? <a href="toernooi_lijst">Toernooi lijst!</a></li>
                    <li>Begin een nieuw toernooi! <a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
                  </ul>
                  
                  <h3>Waarom dit Systeem?</h3>
                  <p>Dit systeem is een project die ik heb gemaakt voor een project op mijn school. Tijdens de opleiding krijgen we het vak individual propedeuse assessment bij dit vak mochten we zelf een project bedenken. Ik heb daarom voor het toernooi management systeem gekozen.</p>
				  
				  <p>Er zijn nog weinig webapplicatie systemen die het ondersteuen van toernooien niet goed ondersteuen. Dit simpelen systeem gaat er voor zorgen dat je makkelijk met je vrienden een toernooi kan orginiseren.</p>
            </section>

          </section>
          <section class="col-xs-12 col-sm-6 col-md-6">

                <h2>Een aantal functies</h2>

                <h3>Toernooi toevoegen</h3>
                <p>Met een deze funtie kan je een eigen toernooi aanmaken. Hiervoor kan je <a href="#">Hier!</a>klikken. Jij en de deelnemers kunnen zelf het toernooi beheren.</p>
                
                <p>Binnen het toernooi kan je ook wijzigen, als je bijvoorbeeld iets verkeerd heb ingevoerd tijden het toevoegen van een toernooi.</p>
                
                <h3>Toernooi Lijst</h3>
                <p>Als je graag wilt deelnemer maar je weet niet met wie. Kan je altijd deelnemen aan een toernooi die is aangemaakt. Dit kan je doen door op toernooi lijst te drukken. Hierin kan je zelf een toernooi uitkiezen die jij wel aanstaat.</p>
                
                <h2>Beheren van een toernooi</h2>
                <p>Je kan makkelijk een toernoo beheren. Als binnnen een toernooi zit kan je het toernooi wijzigen en verwijderen. Je kan in het toernooi ook wedstrijden toevoegen en wijzigen</p>
                
                

          </section>
        </div>
        
</section>
</body>
</html>