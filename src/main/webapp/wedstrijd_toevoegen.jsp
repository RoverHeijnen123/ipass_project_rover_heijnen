<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import ="ipass.dao.domain.gebruikers" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="ipass.domain.service.ServiceProvider"%>
<% gebruikers g = (gebruikers) request.getSession().getAttribute("loggedUser"); %>
<% request.setAttribute("wedstrijdlist", ServiceProvider.getToernooiService().getWedstrijdList()); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>
<section class="container">
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; margin-bottom: 0px;">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<a class="navbar-brand" href="index.jsp">Toernooi management Systeem</a>
    		</div>
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="index.jsp">Home</a></li>
			    <li><a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
			    <li><a href="toernooi_lijst.jsp">Toernooi lijst</a></li> 
    		</ul>
    		<ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-user"></span>User: <%=g.getGebruikersnaam() %> </a></li>
		      <li><span class="glyphicon glyphicon-log-out"></span><form action="LogoutServlet.do" method="get"><input class="glyphicon glyphicon-log-out" type="submit" name="logout" value="logout"></form></li>
    		</ul>
  		</div>
	</nav>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; border-top: 1px solid gray;">
  		<div class="container-fluid">
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="toernooi_wijzigen.jsp">Toernooi Wijzigen</a></li>
			    <li><a href="ToernooiVerwijderen.jsp">Toernooi Verwijderen</a></li>
			    <li><a href="wedstrijd_toevoegen.jsp">Wedstrijden Toevoegen</a></li>
			    <li><a href="mijn_toernooi.jsp">Wedstrijden Overzicht</a></li> 
			    <li><a href="deelnemer_toevoegen.jsp">Deelnemers Toevoegen</a></li>
			    <li><a href="wedstrijd_wijzigen.jsp">Wedstrijd Wijzigen</a></li>    
    		</ul>
  		</div>
	</nav>
	
	<form class="form-horizontal" role="form" action="wedstrijdToevoegenServlet.do" method="post">
	<div class="form-group">
    <label class="control-label col-sm-2" for="speler1">wedstrijd Id:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="wedstrijdid" placeholder="wedstrijdid" name="wedstrijdid">
    </div>
  </div>
	<div class="form-group">
    <label class="control-label col-sm-2" for="speler1">Wedstrijd naam:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="wedstijdnaam" placeholder="wedstrijdnaam" name="naam">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="speler1">speler1:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="speler1" placeholder="speler1" name="speler1">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="punten1">Punten speler1:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="punten1" placeholder="punten1" name="punten1">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="speler2">speler2:</label>
    <div class="col-sm-10"> 
      <input type="text" class="form-control" id="speler2" placeholder="speler2" name="speler2">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="punten2">Punten speler2:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="punten2" placeholder="punten2" name="punten2">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="begindatum">Datum:</label>
    <div class="col-sm-10"> 
      <input type="date" class="form-control" id="begindatum" placeholder="Begin Datum" name="datum">
    </div>
  </div>
   
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>
        
</section>
</body>
</html>