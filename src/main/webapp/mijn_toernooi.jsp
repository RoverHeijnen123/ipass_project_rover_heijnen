<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="ipass.domain.service.ServiceProvider"%>
<%@page import ="ipass.dao.domain.gebruikers" %>
<% gebruikers g = (gebruikers) request.getSession().getAttribute("loggedUser"); %>
<% request.setAttribute("deelnemerList", ServiceProvider.getToernooiService().getDeelnemerList()); %>
<% request.setAttribute("wedstrijdlist", ServiceProvider.getToernooiService().getWedstrijdList()); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">
<script src="https://code.jquery.com/jquery-2.2.3.js"></script>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/foundation.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>
<section class="container">
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; margin-bottom: 0px;">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<a class="navbar-brand" href="index.jsp">Toernooi management Systeem</a>
    		</div>
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="index.jsp">Home</a></li>
			    <li><a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
			    <li><a href="toernooi_lijst.jsp">Toernooi lijst</a></li>
    		</ul>
    		<ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-user"></span>User: <%=g.getGebruikersnaam() %> </a></li>
		      <li><span class="glyphicon glyphicon-log-out"></span><form action="LogoutServlet.do" method="get"><input class="glyphicon glyphicon-log-out" type="submit" name="logout" value="logout"></form></li>
    		</ul>
  		</div>
	</nav>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; border-top: 1px solid gray;">
  		<div class="container-fluid">
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="toernooi_wijzigen.jsp">Toernooi Wijzigen</a></li>
			    <li><a href="ToernooiVerwijderen.jsp">Toernooi Verwijderen</a></li>
			    <li><a href="wedstrijd_toevoegen.jsp">Wedstrijden Toevoegen</a></li>
			    <li><a href="mijn_toernooi.jsp">Wedstrijden Overzicht</a></li>
			    <li><a href="deelnemer_toevoegen.jsp">Deelnemers Toevoegen</a></li>
			    <li><a href="wedstrijd_wijzigen.jsp">Wedstrijd Wijzigen</a></li>
    		</ul>
  		</div>
	</nav>
	<!-- hallo wereld -->
	
	<div class="row">
          <section class='col-xs-12 col-sm-6 col-md-6'>
            <section>
              	<h2>Toernooi Info</h2>
				<div id="ToernooiInfo"></div>
            </section>

          </section>
          <section class="col-xs-12 col-sm-6 col-md-6">
			<div id = "deelnemers">
				<h2>Deelnemers</h2>
				<c:forEach var="deelnemer" items="${deelnemerList}">
	  				<br>${deelnemer.deelnemer}</br>
	  			</c:forEach>
			</div>
                

          </section>
        </div>
	
	
	<h2>Wedstrijden</h2>
	<div class="list-group">
	  <ul class="list-group">
		  <!-- <a href="#"><li class="list-group-item">Wedstrijd 1<span class="datum">07-06-2016</span><span class="player1">Punten 3 &nbsp Player1</span><span class="vs">VS</span><span class="player2">Player2 &nbsp punten 0</span></li></a> -->
		   <c:forEach var="wedstrijd" items="${wedstrijdlist}">
	  			<li class="list-group-item">${wedstrijd.wedstrijdnaam}<span class="datum">${wedstrijd.datum}</span><span class="player1">Punten: ${wedstrijd.punten1} &nbsp ${wedstrijd.speler1}</span><span class="vs">VS</span><span class="player2">${wedstrijd.speler2} &nbsp punten: ${wedstrijd.punten2}</span></li>
	  		</c:forEach>
	 </ul>
	</div>

	
	<script>
		$(document).ready(
				function() {
					console.log("Ik ga zoeken naar het toernooi..");
					var toernooi = sessionStorage.getItem("toernooi");
					console.log("Gevonden: " + toernooi);

					$.get("restservices/getToernooiInfo/" + toernooi, function(t) {
						console.log(t.ToernooiNaam);
						console.log(t.ToernooiBeschrijving);
						console.log(t.ToernooiGrote);
						console.log(t.BegindDatum);
						console.log(t.Einddatum);
						$("#ToernooiInfo").append(
								"<br>Toernooi naam: <b>" + t.ToernooiNaam + "</b><br>",
								"<br>Beschrijving: <b>" + t.ToernooiBeschrijving + "</b><br>",
								"<br>Grote: <b>" + t.ToernooiGrote + "</b><br>",
								"<br>Begin datum: <b>" + t.BegindDatum + "</b><br>",
								"<br>Eind datum: <b>" + t.Einddatum + "</b><br>"
								)
					});

				});
	</script>
        
</section>
</body>
</html>