<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Toernooi Management Systeem Login</h1>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                    alt="">
                 
                 <%
Object msg_velden = request.getAttribute("msg_velden");
if (msg_velden != null) {
	out.println(msg_velden);
}

Object msg_usernaam = request.getAttribute("msg_usernaam");
if (msg_usernaam != null) {
	out.println(msg_usernaam);
}


%>
                    
                <form class="form-signin" action="LoginServlet.do" method="post">
                	<input type="text" name="gebruikersnaam" class="form-control"  placeholder="Gebruikersnaam" value="" required autofocus>
                	<input type="password" name="wachtwoord" class="form-control" placeholder="Wachtwoord" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                </form>
                
            </div>
            <a href="#" class="text-center new-account">Registreren</a>
        </div>
    </div>
</div>
</body>
</html>