<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import ="ipass.dao.domain.gebruikers" %>
<% gebruikers g = (gebruikers) request.getSession().getAttribute("loggedUser"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>
<section class="container">
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; margin-bottom: 0px;">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<a class="navbar-brand" href="#">Toernooi management Systeem</a>
    		</div>
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="index.jsp">Home</a></li>
			    <li><a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
			    <li><a href="toernooi_lijst.jsp">Toernooi lijst</a></li> 
    		</ul>
    		<ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-user"></span>User: <%=g.getGebruikersnaam() %> </a></li>
		      <li><span class="glyphicon glyphicon-log-out"></span><form action="LogoutServlet.do" method="get"><input class="glyphicon glyphicon-log-out" type="submit" name="logout" value="logout"></form></li>
    		</ul>
  		</div>
	</nav>
	
	<form class="form-horizontal" role="form" action="ToernooiToevoegenServlet.do" method="post">
  <div class="form-group">
    <label class="control-label col-sm-2" for="naam">Naam:</label>
    <div class="col-sm-10">
      <input type="text" name="naam" class="form-control" id="naam" placeholder="Naam" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="beschrijving">Beschrijving:</label>
    <div class="col-sm-10"> 
      <input type="text" name="beschrijving" class="form-control" id="beschrijving" placeholder="Beschrijving" required>
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="begindatum">BeginDatum:</label>
    <div class="col-sm-10"> 
      <input type="date" name="begindatum" class="form-control" id="beschrijving" placeholder="Begin Datum" required>
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="einddatum">EindDatum:</label>
    <div class="col-sm-10"> 
      <input type="date" name="einddatum" class="form-control" id="beschrijving" placeholder="Eind Datum" required>
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="grote">Grote:</label>
    <div class="col-sm-10"> 
      <input type="text" name="grote" class="form-control" id="grote" placeholder="Grote" required>
    </div>
  </div>
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Opslaan</button>
    </div>
  </div>
  <%
		Object msgs = request.getAttribute("msgs");
		if (msgs != null) {
		out.println(msgs);
		}

		Object naam = request.getAttribute("naam");
  %>
</form>
        
</section>
</body>
</html>