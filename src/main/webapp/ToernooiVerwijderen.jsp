<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="ipass.domain.service.ServiceProvider"%>
<%@page import ="ipass.dao.domain.gebruikers" %>
<% gebruikers g = (gebruikers) request.getSession().getAttribute("loggedUser"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--  <link rel="stylesheet" type="text/css" href="bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="css/customstyle.css">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Toernooi Management systeem</title>
</head>
<body>
<section class="container">
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; margin-bottom: 0px;">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<a class="navbar-brand" href="index.jsp">Toernooi management Systeem</a>
    		</div>
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="index.jsp">Home</a></li>
			    <li><a href="toernooi_toevoegen.jsp">Toernooi toevoegen</a></li>
			    <li><a href="toernooi_lijst.jsp">Toernooi lijst</a></li> 
    		</ul>
    		<ul class="nav navbar-nav navbar-right">
		       <li><a href="#"><span class="glyphicon glyphicon-user"></span>User: <%=g.getGebruikersnaam() %> </a></li>
		      <li><span class="glyphicon glyphicon-log-out"></span><form action="LogoutServlet.do" method="get"><input class="glyphicon glyphicon-log-out" type="submit" name="logout" value="logout"></form></li>
    		</ul>
  		</div>
	</nav>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd; border-top: 1px solid gray;">
  		<div class="container-fluid">
    		<ul class="nav navbar-nav">
      			<li class="active"><a href="toernooi_wijzigen.jsp">Toernooi Wijzigen</a></li>
			    <li><a href="ToernooiVerwijderen.jsp">Toernooi Verwijderen</a></li>
			    <li><a href="wedstrijd_toevoegen.jsp">Wedstrijden Toevoegen</a></li>
			    <li><a href="mijn_toernooi.jsp">Wedstrijden Overzicht</a></li> 
			    <li><a href="deelnemer_toevoegen.jsp">Deelnemers Toevoegen</a></li>
			    <li><a href="wedstrijd_wijzigen.jsp">Wedstrijd Wijzigen</a></li>   
    		</ul>
  		</div>
	</nav>
	
	<form action="ToernooiVerwijderenServlet.do" method="post">
			<h2>Toernooi verwijderen</h2>
			<% request.setAttribute("toernooiList", ServiceProvider.getToernooiService().getToernooiList()); %>
			<label>toernooi: </label> 
				<select name="naam">
				  	<c:forEach var="toernooi" items="${toernooiList}">
				  		<option value="${toernooi.naam}"> <c:out value="${toernooi.naam}" /> </option>	
				  	</c:forEach>
				</select>
			<br><br>
			<input class="alert button expanded" type="submit" value="Verwijderen" />
		</form>
        
</section>
</body>
</html>