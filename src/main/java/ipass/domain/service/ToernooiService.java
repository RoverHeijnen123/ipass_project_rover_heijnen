package ipass.domain.service;

import java.util.ArrayList;
import java.util.List;

import ipass.dao.domain.deelnemer;
import ipass.dao.domain.gebruikers;
import ipass.dao.domain.toernooi;
import ipass.dao.domain.wedstrijd;
import ipass.persistence.DeelnemerDAO;
import ipass.persistence.GebruikerDAO;
import ipass.persistence.ToernooiDAO;
import ipass.persistence.WedstrijdDAO;


public class ToernooiService {
	
	private GebruikerDAO gebruikersDAO = new GebruikerDAO();
	private ToernooiDAO toernooiDAO = new ToernooiDAO();
	private WedstrijdDAO wedstrijdDAO = new WedstrijdDAO();
	private DeelnemerDAO deelnemerDAO = new DeelnemerDAO();
	private List<gebruikers> allGebruikers = new ArrayList<gebruikers>();
	public ArrayList<toernooi> toernooiList = toernooiDAO.findAll();
	public ArrayList<wedstrijd> wedstrijdlist = wedstrijdDAO.findAll();
	public ArrayList<deelnemer> deelnemerList = deelnemerDAO.findDeelnemer();
	
	public ArrayList<toernooi> getToernooiList() {
		toernooiList = toernooiDAO.findAll();
		return toernooiList;
	}
	
	public ArrayList<wedstrijd> getWedstrijdList() {
		wedstrijdlist = wedstrijdDAO.findAll();
		return wedstrijdlist;
	}
	
	public ArrayList<deelnemer> getDeelnemerList() {
		deelnemerList = deelnemerDAO.findDeelnemer();
		return deelnemerList;
	}
	
	public List<gebruikers> loginGebruiker(String gebruikersnaam, String wachtwoord) {
		gebruikers g = null;
		for (gebruikers gebruiker : allGebruikers) {
			if(gebruiker.getGebruikersnaam().equals(gebruikersnaam) && gebruiker.checkWachtwoord(wachtwoord)){
				g = gebruiker;
				break;
			} else {
				g = null;
			}
			
		}
		return gebruikersDAO.findGebruiker();
	}
	
	
	public boolean toernooiToevoegen(toernooi toernooi) {
		ArrayList<toernooi> toernooiList = toernooiDAO.findAll();
		
		toernooiDAO.Save(new toernooi(toernooi.getNaam(), toernooi.getBeschrijving(), toernooi.getGrote(), toernooi.beginDatum(), toernooi.eindDatum()));
		return true;	
	}
	
	public boolean wedstrijdToevoegen(wedstrijd wedstrijd) {
		ArrayList<wedstrijd> wedstrijdlist = wedstrijdDAO.findAll();
		
		wedstrijdDAO.SaveWedstrijd(new wedstrijd(wedstrijd.getWedstrijdid(), wedstrijd.getWedstrijdnaam(), wedstrijd.getDatum(), wedstrijd.getSpeler1(), wedstrijd.getSpeler2(), wedstrijd.getPunten1(), wedstrijd.getPunten2()));
		return true;	
	}
	
	public boolean deelnemerToevoegen(deelnemer deelnemer) {
		ArrayList<deelnemer> deelnemerList = deelnemerDAO.findDeelnemer();
		
		deelnemerDAO.SaveDeelnemer(new deelnemer(deelnemer.getDeelnemer()));
		return true;	
	}
	
	public boolean toernooiVerwijderenOpNaam(String naam) {
		toernooi toernooi = toernooiDAO.findByNaam(naam);
		
		toernooiDAO.delete(new toernooi(toernooi.getNaam(), toernooi.getBeschrijving(), toernooi.getGrote(), toernooi.beginDatum(), toernooi.eindDatum()));
		return true;	
	}
	
	public toernooi findToernooiByNaam(String naam) {
		toernooi toernooi = toernooiDAO.findByNaam(naam);
		return toernooi;
	}
	
	public void toevoegenWedstrijdinToernooi(wedstrijd w, toernooi t) {
		wedstrijdDAO.SaveWedstrijd(w);
		System.out.println(w.getWedstrijdnaam() + " is toegevoegd aan toernooi: " + t.getNaam());
	}

	/*public ArrayList<wedstrijd> getWedstrijdvanToernooi(toernooi toernooi) {
		ArrayList<wedstrijd> wedstrijdtoernooi = wedstrijdDAO.getWedstrijdvanToernooi(toernooi);
	
	return wedstrijdtoernooi;
	}*/


	public wedstrijd findWefstrijdByid(int i) {
		wedstrijd a = wedstrijdDAO.findById(i);
		return a;
	}
	
	public boolean updateToernooi(toernooi toernooi) {
		ArrayList<toernooi> toernooiList = toernooiDAO.findAll();
				
		toernooiDAO.Wijzig(new toernooi(toernooi.getNaam(), toernooi.getBeschrijving(), toernooi.getGrote(), toernooi.beginDatum(), toernooi.eindDatum()));
		return true;
	}
	
	public boolean updateWedstrijd(wedstrijd wedstrijd) {
		ArrayList<wedstrijd> wedstrijdlist = wedstrijdDAO.findAll();
				
		wedstrijdDAO.WedstrijdWijzig(new wedstrijd(wedstrijd.getWedstrijdid(), wedstrijd.getWedstrijdnaam(), wedstrijd.getDatum(), wedstrijd.getSpeler1(), wedstrijd.getSpeler2(), wedstrijd.getPunten1(), wedstrijd.getPunten2()));
		return true;
	}
			
		
}

