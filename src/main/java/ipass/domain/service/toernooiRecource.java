package ipass.domain.service;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import ipass.dao.domain.toernooi;
import ipass.dao.domain.wedstrijd;

@Path("getToernooiInfo")
public class toernooiRecource  {
	
	@GET
	@Path("{naam}")
	@Produces("application/json")
	public String getToernooiInfo(@PathParam("naam") String naam) {
		
		System.out.println("Toernooinaam: " + naam);
		toernooi t = ServiceProvider.getToernooiService().findToernooiByNaam(naam);
		
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("ToernooiNaam",  t.getNaam());
		job.add("ToernooiBeschrijving",  t.getBeschrijving());
		job.add("ToernooiGrote",  t.getGrote());
		job.add("BegindDatum", t.beginDatum());
		job.add("Einddatum", t.eindDatum());
		return job.build().toString();
	}

}