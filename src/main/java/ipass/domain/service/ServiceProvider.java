package ipass.domain.service;

public class ServiceProvider {
	
	private static ToernooiService pricateStaticToernooiService = new ToernooiService();
	
	public static ToernooiService getToernooiService(){
		return pricateStaticToernooiService;
	}
	
}
