package ipass.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.dao.domain.toernooi;
import ipass.domain.service.ServiceProvider;
import ipass.persistence.ToernooiDAO;

public class ToernooiToevoegenServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;


	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String naam = req.getParameter("naam");
		String beschrijving = req.getParameter("beschrijving");
		String begindatum = req.getParameter("begindatum");
		String einddatum = req.getParameter("einddatum");
		String grote = req.getParameter("grote");
		
		int igrote = Integer.parseInt(grote);
		

		// Controleer of alles is ingevuld
		if (naam != null && beschrijving != null && begindatum !=null && einddatum != null && grote != null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "Een van de velden is nog leeg");
		}

		// RequestDisPatcher --> Doe dingen na registreren --> Nieuwe gebruiker
		RequestDispatcher rd = null;
		if (succes) {			
			ServiceProvider.getToernooiService().toernooiToevoegen(new toernooi(naam, beschrijving, igrote, begindatum, einddatum));
			
			// Attributes bijweken
			req.setAttribute("msgs", naam + " is succesvol toegevoegd!");
			
			rd = req.getRequestDispatcher("toernooi_lijst.jsp");
		} else {
			rd = req.getRequestDispatcher("toernooi_toevoegen.jsp");
		}
		rd.forward(req, resp);

	}

}
