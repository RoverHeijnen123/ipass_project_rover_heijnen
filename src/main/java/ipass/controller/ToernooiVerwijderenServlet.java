package ipass.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.ServiceProvider;

public class ToernooiVerwijderenServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String naam = req.getParameter("naam");


		// Controleer of alles is ingevuld
		if (naam != null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "De naam is leeg");
		}

		// RequestDisPatcher
		RequestDispatcher rd = null;
		if (succes) {			
				if (ServiceProvider.getToernooiService().toernooiVerwijderenOpNaam(naam)) {
					req.setAttribute("msgs", naam + " is succesvol verwijderd.");
					rd = req.getRequestDispatcher("toernooi_lijst.jsp");
				} else {
					req.setAttribute("msgs", naam + " album verwijderen op naam ging fout");
					rd = req.getRequestDispatcher("toernooi_lijst.jsp");
				}
		} else {
			req.setAttribute("msgs", naam + " verwijderen ging fout");
			rd = req.getRequestDispatcher("toernooi_lijst.jsp");
		}
		
		rd.forward(req, resp);

	}
}
