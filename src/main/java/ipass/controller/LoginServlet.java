package ipass.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.dao.domain.gebruikers;
import ipass.domain.service.ServiceProvider;
import ipass.domain.service.ToernooiService;

public class LoginServlet extends HttpServlet {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String gebruikersnaam = req.getParameter("gebruikersnaam");
		String wachtwoord = req.getParameter("wachtwoord");
		
		/*ServletContext myContext = getServletContext();
		ArrayList<User> userList = (ArrayList<User>) myContext.getAttribute("userList");*/
		boolean loginsucces = false;
		
		ToernooiService service = ServiceProvider.getToernooiService();
		List<gebruikers> loggedUser = service.loginGebruiker(gebruikersnaam, wachtwoord);
		
		if(loggedUser != null) {
			
			//loginsucces = true;
			
			//Cookie ck = new Cookie("gebruikersnaam", req.getParameter("gebruikersnaam"));  
            //resp.addCookie(ck);  
            
            //req.getSession().setAttribute("loggedUser", loggedUser);
			
			for(gebruikers Gebruikersnaam : loggedUser) {
				if (Gebruikersnaam.getGebruikersnaam().equals(gebruikersnaam))  {
					if(	Gebruikersnaam.checkWachtwoord(wachtwoord)) {
						loginsucces = true;
						
						Cookie ck = new Cookie("gebruikersnaam", req.getParameter("gebruikersnaam"));  
			            resp.addCookie(ck);  
			            
			            req.getSession().setAttribute("loggedUser", Gebruikersnaam);
					} else {
						req.setAttribute("msg_wachtwoord", "Wachtwoord is onjuist");
					}
				} else {
					req.setAttribute("msg_usernaam", "Username is onjuist");
				}
				
			
		//} else {
		//	req.setAttribute("msg_velden", "Gegegevens zijn onjuist");
		}
		
		RequestDispatcher rd = null;
		if (loginsucces){ 
			rd = req.getRequestDispatcher("index.jsp");
		}else {
			rd = req.getRequestDispatcher("login.jsp");
		}
		rd.forward(req, resp);	
		
		
		
	}
	}
}

