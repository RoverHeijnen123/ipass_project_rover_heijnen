package ipass.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.dao.domain.toernooi;
import ipass.dao.domain.wedstrijd;
import ipass.domain.service.ServiceProvider;

public class wedstrijdToevoegenServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;


	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String wedstrijdid = req.getParameter("wedstrijdid");
		String naam = req.getParameter("naam");
		String datum = req.getParameter("datum");
		String speler1 = req.getParameter("speler1");
		String speler2 = req.getParameter("speler2");
		String punten1 = req.getParameter("punten1");
		String punten2 = req.getParameter("punten2");
		
		int id = Integer.parseInt(wedstrijdid);
		int p1 = Integer.parseInt(punten1);
		int p2 = Integer.parseInt(punten2);
		

		// Controleer of alles is ingevuld
		if (naam != null && wedstrijdid != null && datum !=null && speler1 !=null && speler2 !=null && punten1 !=null && punten2 !=null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "Een van de velden is nog leeg");
		}

		// RequestDisPatcher
		RequestDispatcher rd = null;
		if (succes) {			
			ServiceProvider.getToernooiService().wedstrijdToevoegen(new wedstrijd(id, naam, datum, speler1, speler2, p1, p2));
			
			// Attributes bijweken
			req.setAttribute("msgs", naam + " is succesvol toegevoegd!");
			
			rd = req.getRequestDispatcher("mijn_toernooi.jsp");
		} else {
			rd = req.getRequestDispatcher("wedstrijd_toevoegen.jsp");
		}
		rd.forward(req, resp);

	}
}
