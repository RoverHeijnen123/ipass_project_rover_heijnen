package ipass.dao.domain;

public class wedstrijd {

	private int wedstrijdid;
	private String wedstrijdnaam;
	private String datum;
	private String speler1;
	private String speler2;
	private int punten1;
	private int punten2;
	
	public wedstrijd(int wid, String wnm, String dm, String s1, String s2, int p1, int p2) {
		wedstrijdid = wid;
		wedstrijdnaam = wnm;
		datum = dm;	
		speler1 = s1;
		speler2 = s2;
		punten1 = p1;
		punten2 = p2;
	}
	
	public int getWedstrijdid() {
		return wedstrijdid;
	}
	
	public String getWedstrijdnaam() {
		return wedstrijdnaam;
	}
	
	public String getDatum() {
		return datum;
	}
	
	public String getSpeler1() {
		return speler1;
	}
	
	public String getSpeler2() {
		return speler2;
	}
	
	public int getPunten1() {
		return punten1;
	}
	
	public int getPunten2() {
		return punten2;
	}
	
	
	public static void add(wedstrijd w) {
		// TODO Auto-generated method stub
		
	}

}
