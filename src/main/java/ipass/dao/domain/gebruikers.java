package ipass.dao.domain;

public class gebruikers {
	private String gebruikersnaam;
	private String naam;
	private String wachtwoord;
	
	public gebruikers(String gb, String ww, String nm) {
		gebruikersnaam = gb;
		naam = nm;
		wachtwoord = ww;
	}
	
	public String getNaam(){
		return naam;
	}
	
	public String getGebruikersnaam(){
		return gebruikersnaam;
	}
	
	public boolean checkWachtwoord(String ww) {
		return wachtwoord.equals(ww);
	}
}
