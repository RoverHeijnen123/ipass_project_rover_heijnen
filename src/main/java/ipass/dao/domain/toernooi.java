package ipass.dao.domain;

import java.util.ArrayList;


public class toernooi {
	private String naam;
	private String beschrijving;
	private int grote;
	private String beginDatum;
	private String eindDatum;
	private ArrayList<wedstrijd> wedstrijden = new ArrayList<wedstrijd>();
	
	public toernooi (String nm, String bsr, int gr, String bD, String eD) {
		naam = nm;
		beschrijving  = bsr;
		grote = gr;
		beginDatum = bD;
		eindDatum = eD;
		@SuppressWarnings("unused")
		ArrayList<wedstrijd> wedstrijden = new ArrayList<wedstrijd>();
	}
	
	public String getNaam() {
		return naam;
	}
	
	public String getBeschrijving() {
		return beschrijving;
	}
	
	public int getGrote() {
		return grote;
	}
	
	public String beginDatum(){
		return beginDatum;
	}
	
	public String eindDatum(){
		return eindDatum;
	}
	
	public void addWedstrijd(wedstrijd w) {
		wedstrijd.add(w);
	}
	
	public ArrayList<wedstrijd> getWedstrijd() {
		return this.wedstrijden;
	}
}
