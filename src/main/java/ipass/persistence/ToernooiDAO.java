package ipass.persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import ipass.dao.domain.toernooi;

public class ToernooiDAO extends BaseDAO {

	private ArrayList<toernooi> selectToernooi(String query) {
		ArrayList<toernooi> results = new ArrayList<toernooi>();
		
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String naam = dbResultSet.getString("naam");
				String beschrijving = dbResultSet.getString("beschrijving");
				int grote = dbResultSet.getInt("grote");
				String beginDatum = dbResultSet.getString("beginDatum");
				String eindatum = dbResultSet.getString("eindatum");


				results.add(new toernooi(naam, beschrijving, grote, beginDatum, eindatum));
				System.out.println("selectToernooi Result added: " + naam);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}
	
	// Save statement (Save toernooi in Database)
		public void Save(toernooi toernooi) {
			String query = "INSERT INTO toernooi(naam, beschrijving, grote, begindatum, eindatum) VALUES ('" + toernooi.getNaam() + "', '" + toernooi.getBeschrijving() + "' , '" + toernooi.getGrote() + "' , '" + toernooi.beginDatum() + "' , '" + toernooi.eindDatum() + "');";
			System.out.println(query);
			
			try (Connection con = getConnection()) {
				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol geupdate
					System.out.println("Succesvol gesaved");
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
			
		}
		
		//Wijzig Toernooi
		public void Wijzig(toernooi toernooi) {
			try (Connection con = getConnection()) {
				String query = "update toernooi set naam ='" + toernooi.getNaam() + "', beschrijving ='" + toernooi.getBeschrijving() + "', grote ='" + toernooi.getGrote() + "', begindatum ='" + toernooi.beginDatum() + "', eindatum ='" + toernooi.eindDatum() + "' where naam = '" + toernooi.getNaam() + "';";
				System.out.println(query);
				
				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol geupdate
					System.out.println("Succesvol geupdated");
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		
		// Find all()
		public ArrayList<toernooi> findAll() {
			return selectToernooi("SELECT * FROM toernooi");
		}
		
		// Find by naam
		public toernooi findByNaam(String naam) {
			String query = "SELECT * FROM toernooi WHERE naam = '" + naam + "';";
			System.out.println("ToernooiDAO: Find by naam Query: " + query);
			return selectToernooi(query).get(0);
		}
		
		public boolean delete(toernooi toernooi) {
			boolean result = false;
			boolean artiestExists = findByNaam(toernooi.getNaam()) != null;

			if (artiestExists) {
				String query = "DELETE FROM toernooi WHERE naam = '" + toernooi.getNaam() + "';";
				System.out.println("toernooi Delete Query: " + query);
				try (Connection con = getConnection()) {

					Statement stmt = con.createStatement();
					if (stmt.executeUpdate(query) == 1) { // 1 row updated!
						// Succesvol gedelete
						result = true;
					}
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}

			return result;
		}
}
