package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ipass.dao.domain.gebruikers;

public class GebruikerDAO extends BaseDAO {
	
	private List<gebruikers> selectgebruikers(String query) {
		List<gebruikers> results = new ArrayList<gebruikers>();
		
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while (dbResultSet.next()) {
				String gebruikersnaam = dbResultSet.getString("gebruikersnaam");
				String wachtwoord = dbResultSet.getString("wachtwoord");
				String naam = dbResultSet.getString("naam");
				
				results.add(new gebruikers(gebruikersnaam, wachtwoord, naam));
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}
	
	public List<gebruikers> findGebruiker() {
		return selectgebruikers("SELECT * FROM gebruikers");
	}
		
}
