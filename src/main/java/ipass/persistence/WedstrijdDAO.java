package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ipass.dao.domain.toernooi;
import ipass.dao.domain.wedstrijd;


public class WedstrijdDAO extends BaseDAO {
	
	private ArrayList<wedstrijd> selectWedstrijd(String query) {
		ArrayList<wedstrijd> results = new ArrayList<wedstrijd>();
		
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				int wedstrijdid = dbResultSet.getInt("wedstrijdid");
				String naam = dbResultSet.getString("naam");
				String datum = dbResultSet.getString("datum");
				String speler1 = dbResultSet.getString("speler1");
				String speler2 = dbResultSet.getString("speler2");
				int punten1 = dbResultSet.getInt("punten1");
				int punten2 = dbResultSet.getInt("punten2");

				results.add(new wedstrijd(wedstrijdid, naam, datum, speler1, speler2, punten1, punten2));
				System.out.println("selectWedstrijd Result added: " + naam + wedstrijdid);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	
	}
	
	// Save statement (Save toernooi in Database)
	public void SaveWedstrijd(wedstrijd wedstrijd) {
		String query = "INSERT INTO wedstrijden(wedstrijdid, naam, datum, speler1, speler2, punten1, punten2) VALUES ('" + wedstrijd.getWedstrijdid() + "', '" + wedstrijd.getWedstrijdnaam() + "', '" + wedstrijd.getDatum() + "', '" + wedstrijd.getSpeler1() + "', '" + wedstrijd.getSpeler2() + "', '" + wedstrijd.getPunten1() + "', '" + wedstrijd.getPunten2() + "');";
		System.out.println(query);
				
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
				
	}
	
	//Wijzig wedstrijden
			public void WedstrijdWijzig(wedstrijd wedstrijd) {
				try (Connection con = getConnection()) {
					String query = "update wedstrijden set wedstrijdid ='" + wedstrijd.getWedstrijdid() + "', naam ='" + wedstrijd.getWedstrijdnaam() + "', datum ='" + wedstrijd.getDatum() + "', speler1 ='" + wedstrijd.getSpeler1() + "', speler2 ='" + wedstrijd.getSpeler2() + "', punten1 ='" + wedstrijd.getPunten1() + "', punten2 ='" + wedstrijd.getPunten2() + "'  where wedstrijdid = '" + wedstrijd.getWedstrijdid() + "';";
					System.out.println(query);
					
					Statement stmt = con.createStatement();
					if (stmt.executeUpdate(query) == 1) { // 1 row updated!
						// Succesvol geupdate
						System.out.println("Succesvol geupdated");
					}
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
	
			// Find all()
			public ArrayList<wedstrijd> findAll() {
				return selectWedstrijd("SELECT * FROM wedstrijden");
			}
			
			// Find by id
			public wedstrijd findById(int wedstrijdid) {
				String query = "SELECT * FROM wedstrijden WHERE wedstrijdid = '" + wedstrijdid + "';";
				System.out.println("WedstrijdDAO: Find by naam Query: " + query);
				return selectWedstrijd(query).get(0);
			}
			
			
			
			/*public ArrayList<wedstrijd> getWedstrijdvanToernooi(toernooi toernooi) {
				String query = "SELECT * FROM wedstrijden a, toernooi b WHERE b.naam = '" + toernooi.getNaam() + "' AND a.wedstrijdid = b.wedstrijdid" + ";";
				System.out.println("Find by naam Query: " + query);
				
				ArrayList<wedstrijd> results = new ArrayList<wedstrijd>();

				try (Connection con = super.getConnection()) {
					Statement stmt = con.createStatement();
					ResultSet dbResultSet = stmt.executeQuery(query);

					while (dbResultSet.next()) {
						int wedstrijdid = dbResultSet.getInt("wedstrijdid");
						String naam = dbResultSet.getString("naam");
						String datum = dbResultSet.getString("datum");
						String speler1 = dbResultSet.getString("datum");
						String speler2 = dbResultSet.getString("datum");

						results.add(new wedstrijd(wedstrijdid, naam, datum, speler1, speler2));
						System.out.println("selectAlbum Result added: " + naam);
					}
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}

				return results;
			}*/
}
